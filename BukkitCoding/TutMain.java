package ru.ibusewinner.busesbots.tutor;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class TutMain extends JavaPlugin implements CommandExecutor {
	
	public static File f;
	public static FileConfiguration cfg;
	
	@Override
	public void onEnable() {
		f = new File(getDataFolder() + "config.yml");
		cfg = getConfig();
		registerEventsAndCommands();
		loadCfg();
		Episode4.registerScoreBoard();
		
		Episode6.timer();
		
		
		sendMessageToConsole("�aPlugin enabled!");
	}

	@Override
	public void onDisable() {
		sendMessageToConsole("�cPlugin disabled!");
	}
	
	public static void sendMessageToConsole(String msg) {
		Bukkit.getConsoleSender().sendMessage(msg);
	}
	
	public void registerEventsAndCommands() {
		/*Bukkit.getPluginManager().registerEvents(new Episode2(), this);
		
		Bukkit.getPluginCommand("fly").setExecutor(new Episode3());
		Bukkit.getPluginManager().registerEvents(new Episode3(), this);
		
		Bukkit.getPluginManager().registerEvents(new Episode4(), this);
		Bukkit.getPluginCommand("timer").setExecutor(this);
		
		Bukkit.getPluginManager().registerEvents(new Episode5(this), this);*/
		
		Bukkit.getPluginManager().registerEvents(new Episode6(this), this);
	}
	
	public void loadCfg() {
		if(!f.exists()) {
			saveDefaultConfig();
		}else {
			saveConfig();
		}
	}
	
	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] a) {
		
		if(!(s instanceof Player)) {
			TutMain.sendMessageToConsole("�4Command only for players!");
			return false;
		}else {
			if(s.isOp() == false) {
				s.sendMessage("�c� ��� ������������ ���� ��� ������������� ������ �������.");
				return false;
			}else {
				Player p = (Player)s;
				
				if(a.length == 0) {
					s.sendMessage("������� ���������� ������!");
					return false;
				}else {
					Episode4.sec = Integer.parseInt(a[0]);
					p.sendMessage("�2������ �������!");
					timer();
				}
			}
		}
		
		return true;
	}
	
	public void timer() {
		int time = Episode4.sec;
		new BukkitRunnable() {
			
			int secs = Episode4.sec;
			
			@Override
			public void run() {
				if(secs == 0) {
					cancel();
					Bukkit.broadcastMessage("�c����� �����!");
					for(Player p : Bukkit.getOnlinePlayers()) {
						p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP,1.0f,1.0f);
					}
				}else {
					Bukkit.broadcastMessage("�a�������� ������: �5"+secs);
					secs--;
					for(Player p : Bukkit.getOnlinePlayers()) {
						p.playSound(p.getLocation(), Sound.BLOCK_NOTE_GUITAR,1.0f,1.0f);
					}
				}
			}
		}.runTaskTimer(this, 0, time*2);
	}
	
}
