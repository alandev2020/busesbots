package ru.ibusewinner.busesbots.tutor;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class Episode2 implements Listener{
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		String name = player.getName();
		
		event.setJoinMessage("�a����� �b"+name+" �a������������� � �������!");
		
		player.sendMessage("�9����� ���������� �� ������!");
		
		TutMain.sendMessageToConsole("�aPlayer �b"+name+" �ajoined in game.");
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		String name = player.getName();
		
		event.setQuitMessage("�c����� �9"+name+" �c����� � �������!");
		
		TutMain.sendMessageToConsole("�cPlayer �9"+name+" �cleft the server.");
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		Player player = event.getEntity().getPlayer();
		
		player.sendMessage("�d�� ����!");
		
		event.setDeathMessage("�d����� �4"+player.getName()+" �d����.");
		
		event.setKeepInventory(true);
		
		TutMain.sendMessageToConsole("�dPlayer �4"+player.getName()+" �dDied.");
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Block block = event.getBlock();
		
		if(block.getType() == Material.SPONGE) {
			if(event.getPlayer() instanceof Player) {
				int random = (int) (Math.random() * 10);
				
				switch (random) {
				case 0:
					event.getPlayer().sendMessage("�a����! ��� ��������� ������!");
					event.setCancelled(true);
					block.breakNaturally();
					ItemStack diamonds = new ItemStack(Material.DIAMOND, 16);
					event.getPlayer().getInventory().addItem(diamonds);
					break;
				case 1:
					event.getPlayer().sendMessage("�d����! ��� �������� ������!");
					event.setCancelled(true);
					block.breakNaturally();
					event.getPlayer().setFireTicks(20);
					break;
				default:
					event.getPlayer().sendMessage("�8��� ������ �� ��������� =(");
					event.setCancelled(true);
					block.breakNaturally();
					break;
				}
			}
			
		}
	}

}
