package ru.ibusewinner.busesbots.tutor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Episode7 {
	
	public static String host = "127.0.0.1";
	public static String port = "3306";
	public static String database = "tutorial";
	public static String user = "root";
	public static String password = "";
	
	public static Connection con;
	
	public static boolean isConnected() {
		return con != null;
	}
	
	public static void Connect() {
		if(!isConnected()) {
			try {
				con = DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+database+"?autoReconnect=true", user, password);
				TutMain.sendMessageToConsole("§aConnected to MySQL!");
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public static void Disconnect() {
		try {
			con.close();
			TutMain.sendMessageToConsole("§aDisconnected from MySQL!");
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static PreparedStatement getStatement(String sql) {
		if(isConnected()) {
			PreparedStatement ps;
			
			try {
				ps = con.prepareStatement(sql);
				return ps;
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}
	
	public static ResultSet getResult(String sql) {
		if(isConnected()) {
			PreparedStatement ps;
			ResultSet rs;
			
			try {
				ps = getStatement(sql);
				rs = ps.executeQuery();
				return rs;
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}
	
}
