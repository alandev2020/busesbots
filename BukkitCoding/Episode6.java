package ru.ibusewinner.busesbots.tutor;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class Episode6 implements Listener{
	
	public static TutMain main;
	
	public Episode6(TutMain maincl) {
		main = maincl;
	}

	public static HashMap<UUID, Integer> cd = new HashMap<UUID, Integer>();
	public static int time = 10;
	
	public static void timer() {
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if(cd.isEmpty()) {
					return;
				}else {
					for(UUID uuid : cd.keySet()) {
						int timer = cd.get(uuid);
						
						if(timer <= 0) {
							cd.remove(uuid);
						}else {
							cd.put(uuid, timer-1);
						}
					}
				}
			}
			
		}.runTaskTimer(main, 0, 20);
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {
		Player p = e.getPlayer();
		Block b = e.getBlock();
		UUID uuid = p.getUniqueId();
		
		if(b.getType().equals(Material.EMERALD_BLOCK)) {
			if(!cd.containsKey(uuid)) {
				cd.put(uuid, time);
				p.sendMessage("�6�� ��������� �a���������� ����6. ������ ��� ����� ��������� 10 ������, ������ ��� ��� ����� ����� ������� �a���������� ����6.");
			}else {
				e.setCancelled(true);
				p.sendMessage("�c��� ���������� ��������� �4"+cd.get(uuid)+" �c������, ������ ��� ������� �a���������� �����c!");
			}
		}
	}
	
}
