package ru.ibusewinner.busesbots.tutor;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.BanList.Type;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class Episode5 implements Listener{
	
	public TutMain plugin;
	public static Inventory banorkick = Bukkit.createInventory(null, 9, "�a�� ������ ��� ��� ���?");
	
	public Episode5(TutMain main) {
		this.plugin = main;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		
		ItemStack ban = new ItemStack(Material.DIAMOND_SWORD, 32);
		ItemMeta ban_meta = ban.getItemMeta();
		ban_meta.setDisplayName("�c� ���� ���!");
		ban.setItemMeta(ban_meta);
			
		ItemStack kick = new ItemStack(Material.CHORUS_PLANT, 16);
		ItemMeta kick_meta = kick.getItemMeta();
		kick_meta.setDisplayName("�6� ���� ���!");
		kick.setItemMeta(kick_meta);
			
		banorkick.setItem(3, ban);
		banorkick.setItem(5, kick);
			
		new BukkitRunnable() {
			
			@Override
			public void run() {
				p.openInventory(banorkick);
			}
		}.runTaskLater(plugin, 20);
	}
	
	@EventHandler
	public void onItemUse(InventoryClickEvent e) {
		try {
			Player p = (Player) e.getWhoClicked();
			
			if(!(e.getClickedInventory().getName() == "�a�� ������ ��� ��� ���?")) {
				return;
			}else {
				if(e.getSlot() == 3) {
					p.kickPlayer("�4�� ������� ���!");
					Bukkit.getBanList(Type.NAME).addBan(p.getName(), "�4�� ������� ���!", null, null);
				}else if(e.getSlot() == 5) {
					p.kickPlayer("�6�� ������� ���!");
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
}
