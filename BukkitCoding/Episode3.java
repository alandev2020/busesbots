package ru.ibusewinner.busesbots.tutor;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class Episode3 implements CommandExecutor, Listener{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("�cCommand only for players!");
			return false;
		}else {
			Player p = (Player)sender;
			
			if(p.isOp() == false) {
				p.sendMessage("�c������ ������� ���������� ������� ��� ����!");
				return false;
			}else {
				if(p.getAllowFlight() == false) {
					p.setAllowFlight(true);
					p.setFlying(true);
					p.sendMessage("�a���� �������!");
				}else {
					p.setAllowFlight(false);
					p.setFlying(false);
					p.sendMessage("�a���� ��������!");
				}
				
			}
		}
		return true;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		
		String name = p.getName();
		int online = Bukkit.getOnlinePlayers().size();
		
		TutMain.cfg.set("online", online);
		TutMain.cfg.set("last-joined", name);
		try {
			TutMain.cfg.save(TutMain.f);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

}
